package com.hcl.customersurveyservice.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.customersurveyservice.dto.CustomerList;
import com.hcl.customersurveyservice.service.CustomerSurveyService;
 
@ExtendWith(SpringExtension.class)
class CustomerSurveyControllerTest {
 
	@Mock
	private CustomerSurveyService customerSurveyService;
	@InjectMocks
	private CustomerSurveyController customerSurveyController;
	@Test
	void testCustomerSurveyController() {
		List<CustomerList> customerList=new ArrayList<>();
		CustomerList customerList2=new CustomerList(12l, "electronic city", 12, 1l);
		customerList.add(customerList2);
		Mockito.when(customerSurveyService.getCustomerList(1l, 10)).thenReturn(customerList);
		ResponseEntity<List<CustomerList>> responseEntity=customerSurveyController.getCustomerForSurvey(1l, 10);
		assertNotNull(responseEntity);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	} 
}