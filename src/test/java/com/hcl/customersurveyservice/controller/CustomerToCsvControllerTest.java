package com.hcl.customersurveyservice.controller;

import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.customersurveyservice.service.CustomerToCsvService;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import jakarta.servlet.http.HttpServletResponse;

@ExtendWith(SpringExtension.class)
class CustomerToCsvControllerTest {
	@Mock
	CustomerToCsvService customerToCsvService;
	@InjectMocks
	CustomerToCsvController csvController;
	
	
	@Mock
	HttpServletResponse response;
	@Test
	void testSuccess() throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {
	
		
	customerToCsvService.storeCustomerSurvey(1L, response);
	verify(customerToCsvService).storeCustomerSurvey(1L, response);
	
	
	}
}
