package com.hcl.customersurveyservice.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.customersurveyservice.dto.ApiResponse;
import com.hcl.customersurveyservice.dto.CustomerRecord;
import com.hcl.customersurveyservice.service.CustomerService;

@ExtendWith(SpringExtension.class)
class CustomerControllerTest {
	@Mock
	private CustomerService customerService;
	@InjectMocks
	private CustomerController customerController;
 
	@Test
	void testCustomer() {
		CustomerRecord customerRecord = new CustomerRecord("Nadiya", "N", "FEMALE", "nadiya03@gmail.com",
				"Electronic city banglore");
		ApiResponse expectedApiResponse = new ApiResponse("Information added successfully", 201l);
 
		when(customerService.register(customerRecord)).thenReturn(expectedApiResponse);
 
		ResponseEntity<ApiResponse> response = customerController.register(customerRecord);
 
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(expectedApiResponse, response.getBody());
	}
 
	
}