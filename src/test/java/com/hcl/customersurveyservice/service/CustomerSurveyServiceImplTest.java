package com.hcl.customersurveyservice.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.customersurveyservice.dto.Cordinate;
import com.hcl.customersurveyservice.dto.CustomerList;
import com.hcl.customersurveyservice.entity.Branch;
import com.hcl.customersurveyservice.entity.Customer;
import com.hcl.customersurveyservice.entity.CustomerSurvey;
import com.hcl.customersurveyservice.exception.BranchNotFound;
import com.hcl.customersurveyservice.feign.CordinateService;
import com.hcl.customersurveyservice.repository.BranchRepository;
import com.hcl.customersurveyservice.repository.CustomerRepository;
import com.hcl.customersurveyservice.repository.CustomerSurveyRepository;
import com.hcl.customersurveyservice.service.impl.CustomerSurveyServiceImpl;
 
@ExtendWith(SpringExtension.class)
class CustomerSurveyServiceImplTest {
	@Mock
	private CustomerRepository customerRepository;
	@Mock
	private BranchRepository branchRepository;
	@Mock
	private CustomerSurveyRepository customerSurveyRepository;
	@Mock
	private CordinateService cordinateService;
 
	@InjectMocks
	private CustomerSurveyServiceImpl customerSurveyServiceImpl;
 
	@Test
	void testNegativeScenario() {
		Mockito.when(branchRepository.findById(1l)).thenReturn(Optional.empty());
		assertThrows(BranchNotFound.class, () -> customerSurveyServiceImpl.getCustomerList(1l, 10));
	}
 
	@Test
	void testPositive() {
		Branch branch = Branch.builder().branchId(1l).branchAddress("Whitefield").build();
		List<CustomerList> customerList = new ArrayList<>();
		CustomerList customerList2 = new CustomerList(12l, "Electronic city", 18295.49220796077, 1l);
		customerList.add(customerList2);
		Customer customer = Customer.builder().customerId(12l).email("manoj@gmail.com").address("Electronic city")
				.firstName("Manoj").lastName("kumar").build();
		List<Customer> customers = new ArrayList<>();
		customers.add(customer);
		Mockito.when(branchRepository.findById(1l)).thenReturn(Optional.of(branch));
		Mockito.when(customerRepository.findAll()).thenReturn(customers);
		Cordinate cordinate = Cordinate.builder().latitude(42.498165).longitude(78.492491).build();
		Mockito.when(cordinateService.getCoordinate(branch.getBranchAddress())).thenReturn(cordinate);
		Mockito.when(cordinateService.getCoordinate(customer.getAddress())).thenReturn(cordinate);
		CustomerSurvey survey=CustomerSurvey.builder().surveyId(1l).address("Whitefield").branchId(1l).customerId(12l).distance(18295.49220796077).build();
		Mockito.when(customerSurveyServiceImpl.getCustomerList(1l, 10)).thenReturn(customerList);
		List<CustomerList> lists=customerSurveyServiceImpl.getCustomerList(1l, 10);
		assertNotNull(lists);
		
		
	}
}
 