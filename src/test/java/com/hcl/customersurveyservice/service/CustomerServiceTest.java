package com.hcl.customersurveyservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.customersurveyservice.dto.ApiResponse;
import com.hcl.customersurveyservice.dto.CustomerRecord;
import com.hcl.customersurveyservice.exception.CustomerAlreadyExist;
import com.hcl.customersurveyservice.repository.CustomerRepository;
import com.hcl.customersurveyservice.service.impl.CustomerServiceImpl;

@ExtendWith(SpringExtension.class)
class CustomerServiceTest {
	@InjectMocks
	private CustomerServiceImpl customerServiceImpl;
	@Mock
	private CustomerRepository customerRepository;
 
	@Test
	void testRegister_NewCustomer_Success() {
		CustomerRecord customerRecord = new CustomerRecord("John", "Doe", "MALE", "john@example.com",
				"Electronic city banglore");
		Mockito.when(customerRepository.existsByEmail(customerRecord.email())).thenReturn(false);
		ApiResponse response = customerServiceImpl.register(customerRecord);
        assertEquals(201L, response.httpStatus());
		assertEquals("Customer Information Added successfully", response.message());
	}
 
	@Test
	void testRegister_NewCustomer_failure() {
		CustomerRecord customerRecord = new CustomerRecord("John", "Doe", "MALE", "john@example.com",
				"Electronic city banglore");
		Mockito.when(customerRepository.existsByEmail(customerRecord.email())).thenReturn(true);
		assertThrows(CustomerAlreadyExist.class, () -> customerServiceImpl.register(customerRecord));
	}
}
