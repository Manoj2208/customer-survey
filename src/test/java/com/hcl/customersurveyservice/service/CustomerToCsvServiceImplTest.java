package com.hcl.customersurveyservice.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.customersurveyservice.entity.Branch;
import com.hcl.customersurveyservice.entity.Customer;
import com.hcl.customersurveyservice.entity.CustomerSurvey;
import com.hcl.customersurveyservice.exception.BranchNotFound;
import com.hcl.customersurveyservice.repository.BranchRepository;
import com.hcl.customersurveyservice.repository.CustomerRepository;
import com.hcl.customersurveyservice.repository.CustomerSurveyRepository;
import com.hcl.customersurveyservice.service.impl.CustomerToCsvServiceImpl;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import jakarta.servlet.http.HttpServletResponse;

@ExtendWith(SpringExtension.class)
class CustomerToCsvServiceImplTest {
	@Mock
	CustomerSurveyRepository customerSurveyRepository;
	@InjectMocks
	CustomerToCsvServiceImpl customerToCsvServiceImpl;
	@Mock
	CustomerRepository customerRepository;
	@Mock
	BranchRepository branchRepository;
	@Mock
	HttpServletResponse response;
	@Test
	void testSuccess() throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		Long branchId=1L;
		CustomerSurvey survey=new CustomerSurvey(1L, 1L, null, 0, branchId);
		Customer customer=new Customer(1L, null, null, null, null, null, null);
		Mockito.when(branchRepository.findById(branchId)).thenReturn(Optional.of(new Branch()));
		Mockito.when(customerSurveyRepository.findByBranchId(branchId)).thenReturn(Arrays.asList(survey));
		Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));
		StringWriter stringWriter=new StringWriter();
		Mockito.when(response.getWriter()).thenReturn(new PrintWriter(stringWriter));
		customerToCsvServiceImpl.storeCustomerSurvey(branchId, response);
		verify(response).setContentType("text/csv");
			}
	@Test
	void testBranchNotFound() {
		Mockito.when(branchRepository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(BranchNotFound.class, ()->customerToCsvServiceImpl.storeCustomerSurvey(1L, response));
	}
}
