package com.hcl.customersurveyservice.exception;

public class BranchNotFound extends GlobalException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public BranchNotFound() {
		super("Branch not found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public BranchNotFound(String message) {
        super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
    }
}
