package com.hcl.customersurveyservice.exception;

public class CustomerAlreadyExist extends GlobalException {

	private static final long serialVersionUID = 1L;

	public CustomerAlreadyExist() {
		super("Resource Conflict Exists", GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

	public CustomerAlreadyExist(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

}
