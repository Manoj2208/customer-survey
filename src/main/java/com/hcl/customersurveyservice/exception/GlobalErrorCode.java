package com.hcl.customersurveyservice.exception;

public class GlobalErrorCode {
	public static final String ERROR_RESOURCE_NOT_FOUND = "404";
	public static final String ERROR_RESOURCE_CONFLICT_EXISTS = "409";

	private GlobalErrorCode() {
		super();
	}

}
