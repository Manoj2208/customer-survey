package com.hcl.customersurveyservice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomerSurvey {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long surveyId;
	private Long customerId;
	private String address;
	private double distance;
	private Long branchId;
}
