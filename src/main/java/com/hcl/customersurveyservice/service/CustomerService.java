package com.hcl.customersurveyservice.service;

import com.hcl.customersurveyservice.dto.ApiResponse;
import com.hcl.customersurveyservice.dto.CustomerRecord;

public interface CustomerService {
	ApiResponse register(CustomerRecord customerRecord);
}
