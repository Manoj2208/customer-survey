package com.hcl.customersurveyservice.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.util.SloppyMath;
import org.springframework.stereotype.Service;

import com.hcl.customersurveyservice.dto.Cordinate;
import com.hcl.customersurveyservice.dto.CustomerList;
import com.hcl.customersurveyservice.entity.Branch;
import com.hcl.customersurveyservice.entity.Customer;
import com.hcl.customersurveyservice.entity.CustomerSurvey;
import com.hcl.customersurveyservice.exception.BranchNotFound;
import com.hcl.customersurveyservice.feign.CordinateService;
import com.hcl.customersurveyservice.repository.BranchRepository;
import com.hcl.customersurveyservice.repository.CustomerRepository;
import com.hcl.customersurveyservice.repository.CustomerSurveyRepository;
import com.hcl.customersurveyservice.service.CustomerSurveyService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerSurveyServiceImpl implements CustomerSurveyService {
	private final CustomerRepository customerRepository;
	private final BranchRepository branchRepository;
	private final CustomerSurveyRepository customerSurveyRepository;
	private final CordinateService cordinateService;

	@Override
	public List<CustomerList> getCustomerList(Long branchId, int distance) {
		Branch branch = branchRepository.findById(branchId).orElseThrow(BranchNotFound::new);
		log.info("Getting list of customers");
		List<Customer> customers = customerRepository.findAll();
		log.info("Calling Cordination microservice for getting co-ordinates");
		Cordinate cordinate = cordinateService.getCoordinate(branch.getBranchAddress());
		Map<Customer, Cordinate> customerCordinateMap = new HashMap<>();
		customers.forEach(
				customer -> customerCordinateMap.put(customer, cordinateService.getCoordinate(customer.getAddress())));
		Map<Customer, Double> customerRadius = new HashMap<>();
		customerCordinateMap.forEach((key, value) -> customerRadius.put(key, getDistance(cordinate.getLatitude(),
				cordinate.getLongitude(), value.getLatitude(), value.getLongitude())));
		List<CustomerList> customerLists = new ArrayList<>();
		customerRadius.forEach((k, v) -> {
			if (distance <= v) {
				CustomerList customerList = new CustomerList(k.getCustomerId(), k.getAddress(), v, branchId);
				customerLists.add(customerList);
			}
		});
		log.info("Found and persisted the list of customer survey data");
		customerSurveyRepository.saveAll(computeList(customerLists));
		return customerLists;
	}

	public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
		return SloppyMath.haversinMeters(lat1, lon1, lat2, lon2) / 1000;
	}

	public static List<CustomerSurvey> computeList(List<CustomerList> customerLists) {
		return customerLists.stream().map(i -> CustomerSurvey.builder().address(i.address()).branchId(i.branchId())
				.customerId(i.customerId()).distance(i.distance()).build()).toList();

	}
}
