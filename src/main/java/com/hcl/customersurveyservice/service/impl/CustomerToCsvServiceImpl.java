package com.hcl.customersurveyservice.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.hcl.customersurveyservice.dto.CustomerSurveyCsvDto;
import com.hcl.customersurveyservice.entity.Branch;
import com.hcl.customersurveyservice.entity.CustomerSurvey;
import com.hcl.customersurveyservice.exception.BranchNotFound;
import com.hcl.customersurveyservice.repository.BranchRepository;
import com.hcl.customersurveyservice.repository.CustomerRepository;
import com.hcl.customersurveyservice.repository.CustomerSurveyRepository;
import com.hcl.customersurveyservice.service.CustomerToCsvService;
import com.opencsv.ICSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerToCsvServiceImpl implements CustomerToCsvService{
	
	private final CustomerSurveyRepository customerSurveyRepository;
	private final CustomerRepository customerRepository;
	private final BranchRepository branchRepository;
	
	@Override
	public void storeCustomerSurvey(Long branchId,HttpServletResponse response) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
		Branch branch=branchRepository.findById(branchId).orElseThrow(BranchNotFound::new);
		response.setContentType("text/csv");
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION,"attachment;filename=\"customersurvey.csv\"");
		log.info("customersurvey.csv created");
		StatefulBeanToCsv<CustomerSurveyCsvDto> writer = 
				new StatefulBeanToCsvBuilder<CustomerSurveyCsvDto>
					(response.getWriter())
					.withQuotechar(ICSVWriter.NO_QUOTE_CHARACTER)
					.withSeparator(ICSVWriter.DEFAULT_SEPARATOR)
					.withOrderedResults(false).build();
		
		List<CustomerSurvey> customerSurveys=customerSurveyRepository.findByBranchId(branchId);
		List<CustomerSurveyCsvDto> customerSurvey=customerSurveys.stream().map(survey->
			 new CustomerSurveyCsvDto(customerRepository.findById(survey.getCustomerId()).get().getFirstName(), survey.getAddress(), survey.getDistance())
			
		).toList();
		log.info("write into customersurvey.csv for branch ",branch);
		writer.write(customerSurvey);
	}
	
	
}
