package com.hcl.customersurveyservice.service.impl;

import org.springframework.stereotype.Service;

import com.hcl.customersurveyservice.dto.ApiResponse;
import com.hcl.customersurveyservice.dto.CustomerRecord;
import com.hcl.customersurveyservice.entity.Customer;
import com.hcl.customersurveyservice.entity.Gender;
import com.hcl.customersurveyservice.exception.CustomerAlreadyExist;
import com.hcl.customersurveyservice.repository.CustomerRepository;
import com.hcl.customersurveyservice.service.CustomerService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerServiceImpl implements CustomerService {
	private final CustomerRepository customerRepository;
	
 
	@Override
	public ApiResponse register(CustomerRecord customerRecord) {
		log.error("Customer Already Exists");
		if (customerRepository.existsByEmail(customerRecord.email())) {
			throw new CustomerAlreadyExist("Email already exist");
		}
		Customer customer = Customer.builder().firstName(customerRecord.firstName()).lastName(customerRecord.lastName())
				.email(customerRecord.email()).gender(Gender.valueOf(customerRecord.gender())).address(customerRecord.address())
				.build();
		customerRepository.save(customer);
		return ApiResponse.builder().httpStatus(201l).message("Customer Information Added successfully")
				.build();
		
 
	}
 
}