package com.hcl.customersurveyservice.service;

import java.util.List;

import com.hcl.customersurveyservice.dto.CustomerList;

public interface CustomerSurveyService {

	List<CustomerList> getCustomerList(Long branchId, int distance);

}
