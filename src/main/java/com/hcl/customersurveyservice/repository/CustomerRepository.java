package com.hcl.customersurveyservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.customersurveyservice.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	boolean existsByEmail(String email);
}
