package com.hcl.customersurveyservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.customersurveyservice.entity.CustomerSurvey;

public interface CustomerSurveyRepository extends JpaRepository<CustomerSurvey, Long>{
	List<CustomerSurvey> findByBranchId(Long branchId);
}
