package com.hcl.customersurveyservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.customersurveyservice.entity.Branch;

public interface BranchRepository extends JpaRepository<Branch, Long> {

}
