package com.hcl.customersurveyservice.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.customersurveyservice.service.CustomerToCsvService;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class CustomerToCsvController {
	private final CustomerToCsvService customerToCsvService;
	@GetMapping("/customers-csv")
	public void storeCustomerSurvey(@RequestParam(name = "branchId") long branchId,HttpServletResponse response) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException, IOException {
		customerToCsvService.storeCustomerSurvey(branchId, response);
	
	}
	

}
