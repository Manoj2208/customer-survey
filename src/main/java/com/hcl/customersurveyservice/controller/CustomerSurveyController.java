package com.hcl.customersurveyservice.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.customersurveyservice.dto.CustomerList;
import com.hcl.customersurveyservice.service.CustomerSurveyService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/customers")
@RequiredArgsConstructor
public class CustomerSurveyController {
	private final CustomerSurveyService customerSurveyService;

	@GetMapping
	public ResponseEntity<List<CustomerList>> getCustomerForSurvey(@RequestParam Long branchId,
			@RequestParam int distance) {
		return ResponseEntity.status(HttpStatus.OK).body(customerSurveyService.getCustomerList(branchId, distance));
	}
}

