package com.hcl.customersurveyservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.customersurveyservice.dto.CustomerRecord;
import com.hcl.customersurveyservice.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class CustomerController {
	private final CustomerService customerService;
 
	@PostMapping("/customers")
	public ResponseEntity<com.hcl.customersurveyservice.dto.ApiResponse> register(
			@Valid @org.springframework.web.bind.annotation.RequestBody CustomerRecord customerRecord) {
		return ResponseEntity.status(HttpStatus.CREATED).body(customerService.register(customerRecord));
	}
 
}