package com.hcl.customersurveyservice.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hcl.customersurveyservice.dto.Cordinate;

import io.swagger.v3.oas.annotations.Parameter;

@FeignClient(url = "http://localhost:8080/hackathon/coordinate-service", name = "cordinate-service")
public interface CordinateService {
	@GetMapping(value = "/get-by-address")
	public Cordinate getCoordinate(@Parameter(name = "address") @RequestParam String address);
}

