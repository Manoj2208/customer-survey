package com.hcl.customersurveyservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CustomerSurveyServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerSurveyServiceApplication.class, args);
	}

}
