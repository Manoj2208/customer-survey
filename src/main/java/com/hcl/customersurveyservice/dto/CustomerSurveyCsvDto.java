package com.hcl.customersurveyservice.dto;

public record CustomerSurveyCsvDto(String customer,String address,double distance) {

}
