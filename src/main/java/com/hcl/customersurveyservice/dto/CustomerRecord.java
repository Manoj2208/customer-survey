package com.hcl.customersurveyservice.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;

@Builder
public record CustomerRecord(@NotBlank(message = "email is required")@Pattern(regexp = "[a-zA-Z ]+", message = "Name should contain only alphabets") String firstName, String lastName,
		@Pattern(regexp = "(MALE)|(FEMALE)|(NOT_DEFINED)", message = "Gender should be MALE,FEMALE OR NOT_DEFINED") String gender,
		@NotBlank(message = "email is required") @Email(message = "invalid email") String email,
		@NotBlank(message = "address is required") String address) {
 
}