package com.hcl.customersurveyservice.dto;

public record CustomerList(Long customerId, String address, double distance,Long branchId) {

}

