package com.hcl.customersurveyservice.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message, Long httpStatus) {
 
}